# AngularTsStartingProject

1) clone repo

2) install dependencies
`npm i`

3) debug mode
`npm start`
in browser `http://0.0.0.0:4000`

4) build-stage
`npm run build-stage`

5) build-prod
`npm run build-prod`

5) make-svg-sprite
`npm run make-svg-sprite`
