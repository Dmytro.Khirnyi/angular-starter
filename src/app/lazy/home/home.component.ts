import {Component, OnInit} from '@angular/core';

import {HomeService} from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [require('./home.component.scss').toString()]
})
export class HomeComponent implements OnInit {
  constructor(private homeService: HomeService) {}

  ngOnInit() {
    this.homeService.getPosts().subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  addPost() {
    this.homeService.addPost({body: 'body'}).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  putPost() {
    this.homeService.putPost({body: 'body'}).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  patchPost() {
    this.homeService.patchPost({body: 'body'}).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  deletePost() {
    this.homeService.deletePost().subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }
}
