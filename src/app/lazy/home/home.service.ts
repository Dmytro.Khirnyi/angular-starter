import {Injectable} from '@angular/core';
import {ApiService} from '../../core/services/api.service';

@Injectable()
export class HomeService {
  constructor(private apiService: ApiService) {
  }

  getPosts() {
    const options = {
      url: '/posts',
      method: 'GET',
    };

    return this.apiService.sendRequest(options);
  }

  addPost(body: any) {
    const options = {
      url: '/posts',
      method: 'POST',
      body: body
    };

    return this.apiService.sendRequest(options);
  }

  putPost(body: any) {
    const options = {
      url: '/posts/1',
      method: 'PUT',
      body: body
    };

    return this.apiService.sendRequest(options);
  }

  patchPost(body: any) {
    const options = {
      url: '/posts/1',
      method: 'PATCH',
      body: body
    };

    return this.apiService.sendRequest(options);
  }

  deletePost() {
    const options = {
      url: '/posts/1',
      method: 'DELETE',
    };

    return this.apiService.sendRequest(options);
  }
}
