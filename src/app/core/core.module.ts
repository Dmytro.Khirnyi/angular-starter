import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {ApiService} from './services/api.service';
import {StoreService} from './services/store.service';
import {MemoryService} from './services/memory.service';
import {UserService} from './services/user.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    RouterModule
  ],
  providers: [ApiService, StoreService, MemoryService, UserService]
})
export class CoreModule {
}
