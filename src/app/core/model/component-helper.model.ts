import {EventEmitter} from '@angular/core';
import {Subscription} from 'rxjs';

export class ComponentHelper {

  protected readonly subscriptionList: Array<Subscription>;
  protected readonly intervalList: Array<any>;

  constructor() {
    this.subscriptionList = [];
    this.intervalList = [];
  }

  protected createEmitter<T>(): EventEmitter<T> {
    return new EventEmitter<T>();
  }

  protected addSubscription(subscription: Subscription): void {
    this.subscriptionList.push(subscription);
  }

  protected addInterval(interval: any): void {
    this.intervalList.push(interval);
  }

  protected onDestroy(): void {
    this.clearSubscriptions();
    this.clearInterval();
  }

  private clearSubscriptions(): void {
    if (this.subscriptionList) {
      this.subscriptionList.forEach((subscription: Subscription) => {
        subscription.unsubscribe();
      });
    }
  }

  private clearInterval(): void {
    if (this.intervalList) {
      this.intervalList.forEach((interval: any) => {
        clearInterval(interval);
      });
    }
  }
}
