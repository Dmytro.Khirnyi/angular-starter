import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

import {IUser} from '../model/user.model';
import {StoreService} from './store.service';

@Injectable()
export class UserService {
  public user$: BehaviorSubject<IUser>;

  constructor(private storeService: StoreService) {
    this.user$ = new BehaviorSubject<IUser>(null);
  }

  public updateUser(user: IUser, key: string = 'user', isRemember: boolean = false): void {
    if (user) {
      this.storeService.setItem(key, user, isRemember);
    }
  }

  public removeUser(key: string = 'user'): void {
    this.storeService.removeItem(key);
  }
}
