import {Http, URLSearchParams, Headers, Request, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RequestArgs} from '@angular/http/src/interfaces';

declare const ENV_CONFIG;

@Injectable()
export class ApiService {
  private defaultError: IDefError = {message: 'Server error!'};

  constructor(private http: Http) {
  }

  public sendRequest(options): Observable<ApiService> {
    let body;
    switch (options.encoding) {
      case 'url':
        body = ApiService.urlEncodeBody(options.body);
        break;
      case 'fd':
        body = ApiService.formDataBody(options.body);
        break;
      default:
        body = options.body;
    }

    const reqOptionsObject: RequestArgs = {
      method: options.method,
      url: ENV_CONFIG.apiUrl + options.url,
      headers: this.getHeaders(),
      body
    };

    return this.http.request(new Request(reqOptionsObject))
      .map((res: Response) => res.json())
      .catch(this.errorHandler.bind(this));
  }

  private getHeaders(): Headers {
    const headers = new Headers();
    const authToken = null;
    if (authToken) {
      headers.append('x-auth-token', authToken);
    }

    return headers;
  }

  errorHandler(err: Response) {
    if ((err instanceof Response) && ApiService.checkResType(err, err.type.toString())) {
      const errBody = err.json() || {};
      if ((typeof errBody === 'object') && Object.keys(errBody).length) {
        return Observable.throw(errBody);
      }
    }

    return Observable.throw(this.defaultError);
  }

  public static formDataBody(data: any): FormData {
    const fd: FormData = new FormData();
    Object.keys(data).forEach(key => {
      fd.append(key, data[key]);
    });

    return fd;
  }

  public static urlEncodeBody(data: any): URLSearchParams {
    const body: URLSearchParams = new URLSearchParams();
    Object.keys(data).forEach(key => {
      body.set(key, data[key]);
    });

    return body;
  }

  public static checkResType(res: any, type: string): boolean {
    const contentType = res.headers.get('Content-Type');
    return !!contentType && contentType.indexOf(type) !== -1;
  }
}
